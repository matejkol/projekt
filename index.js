const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    const vypis = function (hraci_pole, pole) {
        for(let x=0; x< pole; x++ ){
            process.stdout.write("| ");
            for (var y=0; y<pole; y++) {
                if(hraci_pole[x][y] != 'x' && hraci_pole[x][y] != 'o')
                    hraci_pole [x] [y] = '_';
                process.stdout.write(hraci_pole [x] [y] + " | ");
            }

            process.stdout.write( "\n" );
        }
    }

    const read = require('readline-sync');

    let pozx_x=0,pozy_x=0,pozx_o=0,pozy_o=0;
    const hraci_pole = [];
    let vyhra_x=0, vyhra_o=0;

    const pole = read.question("pocet policek ");
    const pocet_vyhernich  = read.question("pocet vyhernich")

    for(let x=0; x< pole; x++ ){
        process.stdout.write("| ");
        for (var y=0; y<pole; y++) {
            hraci_pole [x] = new Array (pole);
            hraci_pole [x] [y] = '_';
            process.stdout.write(hraci_pole[x][y]+" | ");
        }
        process.stdout.write("\n");
    }

    while(vyhra_x != pocet_vyhernich && vyhra_o != pocet_vyhernich){

        //while(hraci_pole[pozx_x-1][pozy_x-1]==='_'); {
        console.log("HRAC X: ");
        pozx_x = read.question("zadej pozici x:");
        pozy_x = read.question("zadej pozici y:");

        hraci_pole [pozy_x-1] [pozx_x-1] = 'x';

        console.log("HRAC O: ");

        pozx_o = read.question("zadej pozici x:");
        pozy_o = read.question("zadej pozici y:");

        hraci_pole [(pozy_o-1)] [(pozx_o-1)] = 'o';

        console.clear();
        vypis(hraci_pole, pole);

        for(let i=0; i<(pole-1); i++)
            for(let j=0;j<(pole-1);j++)
                if (hraci_pole[i][j]==='x'&&hraci_pole[i][j+1]==='x')
                {
                    vyhra_x++;}

        for(let i=0; i<(pole-1); i++)
            for(let j=0;j<(pole-1);j++)
                if (hraci_pole[i][j]==='o'&&hraci_pole[i][j+1]==='o')
                    vyhra_o++;

        for(let i=0; i<(pole-1); i++)
            for(let j=0;j<(pole-1);j++)
                if (hraci_pole[j][i]==='x'&&hraci_pole[i][j+1]==='x')
                    vyhra_x++;

        for(let i=0; i<(pole-1); i++)
            for(let j=0;j<(pole-1);j++)
                if (hraci_pole[i][j]==='x'&&hraci_pole[i+1][j]==='x')
                    vyhra_x++;

        for(let i=0; i<(pole-1); i++)
            for(let j=0;j<(pole-1);j++)
                if (hraci_pole[i][j]==='o'&&hraci_pole[i+1][j]==='o')
                    vyhra_o++;

    }
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});

/*const vypis = function (hraci_pole, pole) {
    for(let x=0; x< pole; x++ ){
        process.stdout.write("| ");
        for (var y=0; y<pole; y++) {
            if(hraci_pole[x][y] != 'x' && hraci_pole[x][y] != 'o')
                hraci_pole [x] [y] = '_';
            process.stdout.write(hraci_pole [x] [y] + " | ");
        }

        process.stdout.write( "\n" );
    }
}

const read = require('readline-sync');

let pozx_x=0,pozy_x=0,pozx_o=0,pozy_o=0;
const hraci_pole = [];
let vyhra_x=0, vyhra_o=0;

const pole = read.question("pocet policek ");
const pocet_vyhernich  = read.question("pocet vyhernich")

for(let x=0; x< pole; x++ ){
    process.stdout.write("| ");
    for (var y=0; y<pole; y++) {
        hraci_pole [x] = new Array (pole);
        hraci_pole [x] [y] = '_';
        process.stdout.write(hraci_pole[x][y]+" | ");
    }
    process.stdout.write("\n");
}

while(vyhra_x != pocet_vyhernich && vyhra_o != pocet_vyhernich){

    //while(hraci_pole[pozx_x-1][pozy_x-1]==='_'); {
     console.log("HRAC X: ");
    pozx_x = read.question("zadej pozici x:");
    pozy_x = read.question("zadej pozici y:");

    hraci_pole [pozy_x-1] [pozx_x-1] = 'x';

    console.log("HRAC O: ");

    pozx_o = read.question("zadej pozici x:");
    pozy_o = read.question("zadej pozici y:");

    hraci_pole [(pozy_o-1)] [(pozx_o-1)] = 'o';

    console.clear();
    vypis(hraci_pole, pole);

    for(let i=0; i<(pole-1); i++)
        for(let j=0;j<(pole-1);j++)
            if (hraci_pole[i][j]==='x'&&hraci_pole[i][j+1]==='x')
            {
                vyhra_x++;}

    for(let i=0; i<(pole-1); i++)
        for(let j=0;j<(pole-1);j++)
            if (hraci_pole[i][j]==='o'&&hraci_pole[i][j+1]==='o')
                vyhra_o++;

    for(let i=0; i<(pole-1); i++)
        for(let j=0;j<(pole-1);j++)
            if (hraci_pole[j][i]==='x'&&hraci_pole[i][j+1]==='x')
                vyhra_x++;

    for(let i=0; i<(pole-1); i++)
        for(let j=0;j<(pole-1);j++)
            if (hraci_pole[i][j]==='x'&&hraci_pole[i+1][j]==='x')
                vyhra_x++;

    for(let i=0; i<(pole-1); i++)
        for(let j=0;j<(pole-1);j++)
            if (hraci_pole[i][j]==='o'&&hraci_pole[i+1][j]==='o')
                vyhra_o++;

}

/*for(let i=0; i<pole; i++)
    for(let j=0;j<pole;j++)
        if(hraci_pole[i][j]==='x'&&hraci_pole[i][j+1]==='x')
            vyhra_x++;*/